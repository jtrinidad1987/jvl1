import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class SqlTest {

    // JDBC URL, username, and password of MySQL server
    private static final String JDBC_URL = "jdbc:mysql://localhost:3306/your_database";
    private static final String USERNAME = "your_username";
    private static final String PASSWORD = "your_password";

    public static void main(String[] args) {
        try {
            // Load the JDBC driver
            Class.forName("com.mysql.cj.jdbc.Driver");

            // Establish a connection
            try (Connection connection = DriverManager.getConnection(JDBC_URL, USERNAME, PASSWORD)) {
                // Call the generic SQL method
                String sqlQuery = "SELECT * FROM your_table";
                ResultSet resultSet = executeSqlQuery(connection, sqlQuery);

                // Process the result set (this is just a basic example)
                while (resultSet.next()) {
                    // Retrieve data from the result set
                    int id = resultSet.getInt("id");
                    String name = resultSet.getString("name");

                    // Do something with the data (print to console in this example)
                    System.out.println("ID: " + id + ", Name: " + name);
                }
            }
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
    }

    // Generic method to execute SQL queries
    private static ResultSet executeSqlQuery(Connection connection, String sqlQuery) throws SQLException {
        // Prepare and execute the SQL query
        try (PreparedStatement preparedStatement = connection.prepareStatement(sqlQuery)) {
            return preparedStatement.executeQuery();
        }
    }
}
